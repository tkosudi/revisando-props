import React from "react";
import "./App.css";
import Title from "./components/Title";
import SubTitle from "./components/SubTitle";
import Body from "./components/Body";
import Footer from "./components/Footer";

class App extends React.Component {
  render() {
    return (
      <div>
        <Title text="Aprendendo React" feeling=" :P" />
        <SubTitle text="Exercício sobre props." feeling=" s2" />
        <Body text="Prática leva a perfeição!" feeling=" <3" />
        <Footer text="Repetição para praticar!" feeling=" :D" />
      </div>
    );
  }
}

export default App;
