import React from "react";
import SubTitleNote from "./SubTitleNote";

class SubTitle extends React.Component {
  render() {
    return (
      <div>
        <h2>{this.props.text}</h2>
        <div>
          <SubTitleNote
            text="Manda mais repetição"
            feeling={this.props.feeling}
          />
        </div>
      </div>
    );
  }
}

export default SubTitle;
