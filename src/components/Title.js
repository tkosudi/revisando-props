import React from "react";
import TitleNote from "./TitleNote";

class Title extends React.Component {
  render() {
    return (
      <div>
        <h1>{this.props.text}</h1>
        <div>
          <TitleNote text="Repetir mais ainda!" feeling={this.props.feeling} />
        </div>
      </div>
    );
  }
}

export default Title;
